CREATE TABLE PHONE_NUMBERS (
    Id INT IDENTITY(1,1) PRIMARY KEY,
    FullName NVARCHAR(255),
	PhoneNumber VARCHAR(12),
	BirthDate DATE
);
GO

CREATE PROCEDURE SelectAllPhoneNumbers
AS
SELECT FullName, PhoneNumber, BirthDate
FROM PHONE_NUMBERS;
GO

CREATE PROCEDURE SelectIdByPhoneNumber @PhoneNumber VARCHAR(12)
AS
SELECT Id
FROM PHONE_NUMBERS
WHERE PhoneNumber = @PhoneNumber;
GO

CREATE PROCEDURE AddNewPhoneNumber @FullName NVARCHAR(255), @PhoneNumber VARCHAR(12), @BirthDate DATE
AS
INSERT INTO PHONE_NUMBERS (FullName, PhoneNumber, BirthDate)
VALUES (@FullName, @PhoneNumber, @BirthDate);
GO

CREATE PROCEDURE UpdatePhoneNumberById @Id INT, @FullName NVARCHAR(255), @PhoneNumber VARCHAR(12), @BirthDate DATE
AS
UPDATE PHONE_NUMBERS
SET FullName = @FullName, PhoneNumber = @PhoneNumber, BirthDate = @BirthDate
WHERE Id = @Id;
GO

CREATE PROCEDURE DeletePhoneNumber @PhoneNumber VARCHAR(12)
AS
DELETE FROM PHONE_NUMBERS 
WHERE PhoneNumber = @PhoneNumber;
GO