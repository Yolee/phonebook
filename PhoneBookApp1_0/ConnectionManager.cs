﻿using Microsoft.Data.SqlClient;
using System;

namespace PhoneBookApp1_0
{
    public static class ConnectionManager
    {
        private const string connectionString = "Data Source=YOURSERVERSNAME\\SQL2022X;Initial Catalog=PHONE_BOOK_DBB;Integrated Security=true;TrustServerCertificate=true;";
        private static SqlConnection connection;
        public static SqlConnection GetConnection()
        {
            connection = new SqlConnection(connectionString);

            return connection;
        }

        public static void OpenConnection()
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }
        }

        public static void CloseConnection()
        {
            if (connection != null && connection.State != System.Data.ConnectionState.Closed)
            {
                connection.Close();
            }
        }
    }
}