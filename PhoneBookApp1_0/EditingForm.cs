﻿using Microsoft.Data.SqlClient;
using System.Data;

namespace PhoneBookApp1_0
{
    public partial class EditingForm : Form
    {
        private readonly SqlConnection connection;
        private readonly string phoneNum;
        private readonly bool isEditMode;
        public EditingForm()
        {
            InitializeComponent();
            connection = ConnectionManager.GetConnection();
            isEditMode = false;
        }

        public EditingForm(string fullName, string phoneNum, DateTime birthDate)
        {
            InitializeComponent();
            connection = ConnectionManager.GetConnection();
            this.phoneNum = phoneNum;
            tbFullName.Text = fullName;
            tbPhoneNum.Text = phoneNum;
            dtPickerBirthDate.Value = birthDate;
            isEditMode = true;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (isEditMode)
            {
                int id = -1;
                try
                {
                    ConnectionManager.OpenConnection();
                    using (SqlCommand command1 = new("SelectIdByPhoneNumber", connection))
                    {
                        command1.CommandType = CommandType.StoredProcedure;
                        command1.Parameters.AddWithValue("@PhoneNumber", phoneNum);

                        object result = command1.ExecuteScalar();
                        id = Convert.ToInt32(result);
                    }

                    using (SqlCommand command2 = new("UpdatePhoneNumberById", connection))
                    {
                        command2.CommandType = CommandType.StoredProcedure;
                        command2.Parameters.AddWithValue("@ID", id);
                        command2.Parameters.AddWithValue("@FullName", tbFullName.Text);
                        command2.Parameters.AddWithValue("@PhoneNumber", tbPhoneNum.Text);
                        command2.Parameters.AddWithValue("@BirthDate", dtPickerBirthDate.Value.ToString("yyyy-MM-dd"));

                        command2.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error loading data: {ex.Message}");
                }
                finally
                {
                    ConnectionManager.CloseConnection();
                }
            }
            else
            {
                try
                {
                    ConnectionManager.OpenConnection();
                    using (SqlCommand command = new("AddNewPhoneNumber", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@FullName", tbFullName.Text);
                        command.Parameters.AddWithValue("@PhoneNumber", tbPhoneNum.Text);
                        command.Parameters.AddWithValue("@BirthDate", dtPickerBirthDate.Value.ToString("yyyy-MM-dd"));
                        command.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error loading data: {ex.Message}");
                }
                finally
                {
                    ConnectionManager.CloseConnection();
                }
            }
            Close();
        }

        private void EditingForm_Closed(object sender, FormClosedEventArgs e)
        {
            var previousForm = new MainForm();
            previousForm.Show();
        }
    }
}
