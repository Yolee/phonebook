﻿namespace PhoneBookApp1_0
{
    partial class EditingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblFullName = new Label();
            lblPhoneNumber = new Label();
            lblBirthDate = new Label();
            btnSave = new Button();
            tbFullName = new TextBox();
            tbPhoneNum = new TextBox();
            dtPickerBirthDate = new DateTimePicker();
            SuspendLayout();
            // 
            // lblFullName
            // 
            lblFullName.AutoSize = true;
            lblFullName.Location = new Point(12, 15);
            lblFullName.Name = "lblFullName";
            lblFullName.Size = new Size(61, 15);
            lblFullName.TabIndex = 3;
            lblFullName.Text = "Full Name";
            // 
            // lblPhoneNumber
            // 
            lblPhoneNumber.AutoSize = true;
            lblPhoneNumber.Location = new Point(12, 56);
            lblPhoneNumber.Name = "lblPhoneNumber";
            lblPhoneNumber.Size = new Size(88, 15);
            lblPhoneNumber.TabIndex = 4;
            lblPhoneNumber.Text = "Phone Number";
            // 
            // lblBirthDate
            // 
            lblBirthDate.AutoSize = true;
            lblBirthDate.Location = new Point(12, 100);
            lblBirthDate.Name = "lblBirthDate";
            lblBirthDate.Size = new Size(59, 15);
            lblBirthDate.TabIndex = 5;
            lblBirthDate.Text = "Birth Date";
            // 
            // btnSave
            // 
            btnSave.Location = new Point(192, 149);
            btnSave.Name = "btnSave";
            btnSave.Size = new Size(75, 23);
            btnSave.TabIndex = 6;
            btnSave.Text = "Ok";
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Click += BtnSave_Click;
            // 
            // tbFullName
            // 
            tbFullName.Location = new Point(125, 12);
            tbFullName.Name = "tbFullName";
            tbFullName.Size = new Size(319, 23);
            tbFullName.TabIndex = 7;
            // 
            // tbPhoneNum
            // 
            tbPhoneNum.Location = new Point(125, 53);
            tbPhoneNum.Name = "tbPhoneNum";
            tbPhoneNum.Size = new Size(319, 23);
            tbPhoneNum.TabIndex = 8;
            // 
            // dtPickerBirthDate
            // 
            dtPickerBirthDate.Location = new Point(125, 94);
            dtPickerBirthDate.Name = "dtPickerBirthDate";
            dtPickerBirthDate.Size = new Size(319, 23);
            dtPickerBirthDate.TabIndex = 9;
            // 
            // EditingForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(456, 184);
            Controls.Add(dtPickerBirthDate);
            Controls.Add(tbPhoneNum);
            Controls.Add(tbFullName);
            Controls.Add(btnSave);
            Controls.Add(lblBirthDate);
            Controls.Add(lblPhoneNumber);
            Controls.Add(lblFullName);
            Name = "EditingForm";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "EditingForm";
            FormClosed += EditingForm_Closed;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbFullName;
        private TextBox tbPhoneNum;
        private DateTimePicker dtPickerBirthDate;
        private Label lblFullName;
        private Label lblPhoneNumber;
        private Label lblBirthDate;
        private Button btnSave;
    }
}