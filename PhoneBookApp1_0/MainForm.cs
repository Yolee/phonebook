using System;
using System.Data;
using Microsoft.Data.SqlClient;
namespace PhoneBookApp1_0
{
    public partial class MainForm : Form
    {
        private readonly SqlConnection connection;
        private readonly DataTable dataTable;
        public MainForm()
        {
            InitializeComponent();
            connection = ConnectionManager.GetConnection();
            dataTable = new DataTable();
            LoadTable();
        }

        private void LoadTable()
        {
            try
            {
                ConnectionManager.OpenConnection();
                using SqlCommand command = new("SelectAllPhoneNumbers", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new(command);

                dataTable.Clear();
                adapter.Fill(dataTable);
                dataGridPhoneBook.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridPhoneBook.DataSource = dataTable.DefaultView;
                dataGridPhoneBook.Columns["FullName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridPhoneBook.Columns["PhoneNumber"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                dataGridPhoneBook.Columns["BirthDate"].Width = 100;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error loading data: {ex.Message}");
            }
            finally
            {
                ConnectionManager.CloseConnection();
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Hide();
            EditingForm editingForm = new();
            editingForm.ShowDialog();
            LoadTable();
        }

        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewRow selectedRow = dataGridPhoneBook.SelectedRows[0];
            DialogResult result = MessageBox.Show($"Do you want to delete contact with name {selectedRow.Cells["FullName"].Value}?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            
            if ( result == DialogResult.Yes )
            {
                try
                {
                    string phoneNumber = selectedRow.Cells["PhoneNumber"].Value.ToString();
                    ConnectionManager.OpenConnection();
                    using (SqlCommand command = new("DeletePhoneNumber", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PhoneNumber", phoneNumber);
                        command.ExecuteNonQuery();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error loading data: {ex.Message}");
                }
                finally
                {
                    ConnectionManager.CloseConnection();
                }
                LoadTable();
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {
            if (dataGridPhoneBook.Rows.Count > 0)
            {
                DataGridViewRow selectedRow = dataGridPhoneBook.SelectedRows[0];
                string fullName = selectedRow.Cells["FullName"].Value.ToString();
                string phoneNum = selectedRow.Cells["PhoneNumber"].Value.ToString();
                DateTime birthDate = Convert.ToDateTime(selectedRow.Cells["BirthDate"].Value);

                if (!string.IsNullOrEmpty(fullName) && !string.IsNullOrEmpty(phoneNum))
                {
                    Hide();

                    EditingForm editingForm = new(fullName, phoneNum, birthDate);
                    editingForm.Show();
                }
            }
            LoadTable();
        }
    }
}